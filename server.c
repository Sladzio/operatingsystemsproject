//
// Created by root on 08.05.16.
//
#define PROTOCOLPORT 2428   //default protocol port number
#define QUEUESIZE 10        //size of request queue
#define MAXRCVSIZE 200
#define STRSIZE 200

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include "strmap.h"

//Thread safe hashmap for holding pairs(key=<player_name>,value=<ip>)
StrMap *players;
int number_of_connected_players = 0;
//Mutex to prevent race conditions
pthread_mutex_t lock;

int main(int argc, char *argv[]) {
    //Init of hashmap
    players = sm_new(100);
    //pointer to a host table entry
    struct hostent *ptrh;
    //pointer to a protocol table entry
    struct protoent *ptrp;
    //Holds server's addresses
    struct sockaddr_in saddr;
    //Holds client's addresses
    struct sockaddr_in caddr;
    //Socket descriptors
    int sd1, sd2;
    //port number of protocol
    int port;
    //Address length
    int addr_len;
    //Thread ID
    pthread_t tid;
    pthread_mutex_init(&lock, NULL);
    //Parsing port from arguments
    if (argc > 1) {
        port = atoi(argv[1]); /* convert argument to binary*/
    } else {
        port = PROTOCOLPORT; /* use default port number */
    }
    //Reseting socket addresses
    memset((char *) &saddr, 0, sizeof(saddr));
    //Set family to Internet
    saddr.sin_family = AF_INET;
    //Set the local ip address
    saddr.sin_addr.s_addr = INADDR_ANY;
    if (port > 0) {
        //function converts the unsigned short integer hostshort from
        //host byte order to network byte order.
        saddr.sin_port = htons((u_short) port);
    }
    else {
        fprintf(stderr, "Port number error %s/n", argv[1]);
        exit(-1);
    }
    //Map TCP transport protocol name to protocol number
    if (((int) (ptrp = getprotobyname("tcp"))) == 0) {
        fprintf(stderr, "cannot map \"tcp\" to protocol number");
        exit(1);
    }
    //Create a socket
    sd1 = socket(PF_INET, SOCK_STREAM, ptrp->p_proto);
    if (sd1 < 0) {
        fprintf(stderr, "socket creation failed\n");
        exit(1);
    }
    //Bind a local address to the socket
    if (bind(sd1, (struct sockaddr *) &saddr, sizeof(saddr)) < 0) {
        fprintf(stderr, "bind failed\n");
        exit(1);
    }

    // marks  the  socket referred to by sockfd as a passive socket,
    //that is, as a socket that will be used to  accept  incoming  connection
    //requests using accept(2).
    //specifies a size of request queue

    if (listen(sd1, QUEUESIZE) < 0) {
        fprintf(stderr, "listen failed\n");
        exit(1);
    }
    addr_len = sizeof(caddr);

    //Server loop for handling requests
    fprintf(stderr, "Server configured correctly.\nRunning...");
    while (1) {

    }


}